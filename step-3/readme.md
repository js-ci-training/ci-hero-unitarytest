**Author**: Jacques Saraydaryan, All rights reserved
# Step 3: CI for Java application
Note tout le contenu de cette partie est disponible ici (https://gitlab.com/js-ci-training/ci-hero-ci-cd-example)

## 1 Contexte
Afin d'automatiser la mise en oeuvre de tests et la compilation de l'application, nous allons mettre en place une chaîne d'intégration continue pour notre application Java.
Git lab propose un gamme d'outils permettant la mise en place de processus automatiques pour l'intégration continue (e.g vérification de la syntaxe, exécution de tests unitaires, création de livrable...).

Une complète présentation de la CI/CD de Gitlab est disponible ici [https://docs.gitlab.com/ee/ci/](https://docs.gitlab.com/ee/ci/). 
![GitlabCi-CD image]( https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)
*Workflow Gitlab CI-CD*

Pour mettre en place l'intégration continue dans un repository git lab, il suffit d'ajouter un fichier ```.gitlab-ci.yml``` à la racine du répository.
Ce fichier va permettre de configurer tout le process automatique d'intégration continue.

## 2 Présentation de  ```.gitlab-ci.yml```

```.gitlab-ci.yml``` contient l'ensemble des instructions permettant la mise en oeuvre de pipeline d'intégration continue. Un ```pipeline``` est une suite d'opérations (```jobs```) qui va être réalisée automatiquement dès qu'un déclencheur est activé (e.g push sur la branch DEV, MASTER). Ces ```jobs``` peuvent être exécutés en parallèle et regroupés en ```stage``` (étapes regroupant plusieurs ```jobs```). Une complète présentation des ```pipelines``` et des ```stages``` est disponible ici [https://docs.gitlab.com/ee/ci/pipelines.html](https://docs.gitlab.com/ee/ci/pipelines.html).   


## 3 Création d'un pipeline de compilation
- Ici nous allons créer une intégration continue permettant de compiler automatiquement notre code.

- Créer un fichier ```.gitlab-ci.yml``` à la racine de votre projet comme suit:


```yaml
# image docker (container virtuel) pour executer les jobs (e.g effectuer le build de l'application)
image: "maven:3-jdk-8"

# commandes à executer sur le container virtuel (e.g ajout d'outils non dispo sur l'image de base)
before_script:
  - echo "I am a script executed before"

# definition de l'ordre d'execution des jobs 
# (e.g tous les jobs qui ont 'state:build' seront executés en premiers, puis tous les jobs de 'state:test' etc..)
stages:
  - build

# definition d'un job, à quelle étape il sera executé (stage), le script a executé (e.g mvn compile)
job_build:
  stage: build
  script:
    - mvn compile
    
```
- Explications
  - ```image: "maven:3-jdk-8"``` permet de préciser quelle image docker sera utilisée pour exécuter les différents jobs. Note il est possible de spécifier des images spécifiques par jobs également. Ici l'image docker utiliser contiendra maven et un jdk-8.
  ```yaml
  ...
  before_script:
  - echo "I am a script executed before"
  ...
  ```
  - Permet d'exécuter des scripts sur le container docker qui sera utilisé avant la réalisation des jobs (e.g sudo apt-get install nano)

  ```yaml
  ...
  stages:
  - build
  ...
  ```
  - Définit les étapes à réaliser dans ce pipeline. ici l'étape ```build``` sera exécutée en première. Note le nom de chaque étape est libre.

  ```yaml
  job_build:
  stage: build
  script:
    - mvn compile
  ```
  - Précise un job. Ici ce job ```job_build```, sera exécuté à l'étape indiquée par ```stage```. Dans notre cas, le job ```job_build``` s'exécutera à l'étape ```build```. ```script``` permet de préciser l'ensemble des commandes à exécuter durant cette étape. Ici nous allons lancer la commande de compilation d'un projet maven ``` mvn compile ```

- Pour que gitlab puisse executé le code demander il devra avoir accès à des instances (serveurs) dans lesquels le code sera exécuté, ces instances sont appelés des `Runners`. Avant de commiter nos changements vérifier que des `Runners` sont disponibles pour votre projet:
  -   Aller dans `Settings>CI/CD` et étendre `Runners`
  -   Vérifier que la checkbox `Enable shared runners for this project``soit activée

- Commiter votre modification et regarder le résultat de CI/CD dans l'interface web de gitlab.

<img alt="img result compile pipeline" src="./img/first-pipeline.png
" width="600">

## 4 Ajout d'une étape d'exécution de tests automatiques
- Modifier le fichier ```.gitlab-ci.yml``` comme suit:

```yaml
...
stages:
  - build
  - test
...
# definition un nouveau job
job_test:
  stage: test
  script:
    - mvn test
...
```
- Explications:
  ```yaml
  ...
  stages:
    - build
    - test
  ...
  ```
  - Ajout d'une étape ```test``` à notre pipeline
  ```yaml
  ...
  job_test:
  stage: test
  script:
    - mvn test
  ...
  ```
  - création d'un nouveau job ```job_test``` qui s'exécutera à l'étape ```test```.

- Commiter votre modification et regarder le résultat de CI/CD dans l'interface web de gitlab.

## 5 Ajout d'une étape de package et récupération de la version compilée
- Modifier le fichier ```.gitlab-ci.yml``` comme suit:
```yaml
...
stages:
  - build
  - test
  - package
...
# definition un nouveau job
job_package:
  stage: package
  script:
    - mvn package
  artifacts:
    paths:
    - target/*.jar
    expire_in: 1 week
```
- Explications:
  ```yaml
  ...
  stages:
    - build
    - test
    - package
  ...
  ```
  - Ajout d'une étape ```package``` à notre pipeline
  ```yaml
  ...
  job_package:
    stage: package
    script:
      - mvn package
    artifacts:
      paths:
      - target/*.jar
      expire_in: 1 week
  ...
  ```
  - Création d'un nouveau job ```job_package``` qui s'exécutera à l'étape ```package```. Ici l'élément ```artifacts``` permet de préciser les objets à récupérer et à rendre disponible au téléchargement sur l'interface web de Gitlab. ```paths``` indique les éléments à récupérer en utilisant les expréssions régulières. Ici tous les fichiers ```.jar``` du répertoire ```targert``` seront récupérés. La balise ```expire_in``` détermine au bout de combien de temps les fichiers vont être disponibles au téléchargement sur l'interface web de Gitlab.


## 6 Déclenchement uniquement sur la branch master
- Ajouter une commande à la fin de votre fichier ```.gitlab-ci.yml``` afin d'activer le job `job_package` du CI-CD uniquement sur certaines branches

```yaml
...
 # definition un nouveau job
job_package:
  stage: package
  script:
    - mvn package
  artifacts:
    paths:
    - target/*.jar
    expire_in: 1 week

  # Activation du Job de la  CI-CD uniquement sur les branches spécifiées
  only:
    - master

```

