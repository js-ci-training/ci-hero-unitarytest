# Tuto de mise en place de test Unitaires 
## Step 0: Intial code version
[readme](./step-0/readme.md)

## Step 1: First Unitary Test
[readme](./step-1/readme.md)

## Step 2: Advanced Test cases
[readme](./step-2/readme.md)

## Step 3: CI PipeLine
[readme](./step-3/readme.md)