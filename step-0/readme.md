# Intial code version
 The Hero object is available, maven dependency are set

 ## 1- Create your maven project
 Open your favorite IDE (e.g eclipse, intellij) and create a new maven projet called **com.ci.hero** (use a basic pom.xml)


 ## 2- Add dependencies
 Add the following dependencies to your **pom.xml**

 ```xml

<dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.12.1</version>
        </dependency>
        <!-- Info and Tuto here https://www.mkyong.com/java/json-simple-how-to-parse-json/ -->
        <dependency>
            <groupId>com.github.cliftonlabs</groupId>
            <artifactId>json-simple</artifactId>
            <version>3.1.0</version>
        </dependency>
        <dependency>
            <groupId>net.sf.dozer</groupId>
            <artifactId>dozer</artifactId>
            <version>5.5.1</version>
        </dependency>
    </dependencies>

 ```
 These dependencies will be used in next steps.

 Update your maven project.

 ## 2- Create a Hero Class
 Create the **com.ci.hero.model** package
 Create a Hero class as below:
```java
package com.ci.hero.model;

import java.util.Random;

public class Hero {
    private Random rand;
    private String name;
    private String superPower;
    private String elementalAffinity;
    private String elementalVulnerability;
    private int hp;
    private int energy;
    private int attack;
    private float attackStd;
    private int defense;
    private float defenseStd;

    public Hero(String name) {
        this.name = name;
        this.rand = new Random();

    }

    public Hero(String name, String superPower, String elementalAffinity, String elementalVulnerability, int hp, int energy, int attack, float attackVariance, int defense, float defenseStd) {
        this.rand = new Random();
        this.name = name;
        this.superPower = superPower;
        this.elementalAffinity = elementalAffinity;
        this.elementalVulnerability = elementalVulnerability;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.attackStd = attackVariance;
        this.defense = defense;
        this.defenseStd = defenseStd;
    }

    public float attack(){
     return attack;
    }

    public float defense(){
        return defense;
    }
... //all Getter and Setter
}
  ```

  