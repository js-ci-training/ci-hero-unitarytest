# First Unitary Test

Get the content of the **step-0** as starting point

## 1 Create Unitary test for Hero creation
Create the file package **com.ci.hero.model** into the **test** folder

Create the class **HeroCreationTest** as follow:

```java
package com.ci.hero.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class HeroCreationTest {

    @Test
    public void createEmptyHero() throws Exception {
        Hero hero=new Hero("testHero");

        assertTrue(true);
    }

    @Test
    public void createHeroWithParameter() throws Exception {
        assertTrue(true);
    }

}

```

Explanation

```java
    @Test
    public void createEmptyHero() throws Exception {
...
    assertTrue(true);
    }
```

```@Test``` informs the test framework (here junit) that the current function is a test to execute

``` assertTrue(true);``` check if the condition is satisfied (here if we reach this line the condition is automatically satisfied)

## 2 Create Unitary test for Hero element : Tools

Before testing elements of Hero create the following package **com.ci.hero.common** and class **Const** as follow (the class **Const** do not follow java conception best practices):

```java
package com.ci.hero.common;

public class Const {
    public final static int HP_BOUND_MIN=1;
    public final static int HP_BOUND_MAX=100;

    public final static int ENERGY_BOUND_MIN=0;
    public final static int ENERGY_BOUND_MAX=500;

    public final static int ATTACK_BOUND_MIN=1;
    public final static int ATTACK_BOUND_MAX=20;

    public final static int DEFENSE_BOUND_MIN=0;
    public final static int DEFENSE_BOUND_MAX=30;
}

```

Create now a new Test Class file **HeroTest** as follow:

```java
package com.ci.hero.model;

import com.ci.hero.common.Const;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class HeroTest {

    protected Hero heroTest;

    @Before
    public void setUp() {
        heroTest = new Hero("testHero");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void simpleCreation(){
        assertTrue(this.heroTest != null);
    }

}
```

Explanation

```@Before``` inform the test framework that the function below as be executed before **each** test function (**@Test**)

```@After``` inform the test framework that the function below as be executed after **each** test function (**@Test**)

 ```assertTrue(this.heroTest != null);``` check during the test if the heroTest has correctly been created 

## 3 Create Unitary test for Hero element : Test

We want our programme to fit some requirements. To do so, we will processed to Test Driven Development, tests will be written before the features.

Complete the test class **HeroTest** to:
* test that the distrubution of attack and defence function is correct (e.g 90% of attack value with attack=10 attackStd=1 has to be between 9 and 11 value)
* test the bound of each Hero attributes (**hp**,**energy**,**attack**,**defense**), meaning that if a value is set to an attribute if the value is above the max bound the attribute value **must** get the max bound value. 

Then modified the Hero class to fit the HeroTest case