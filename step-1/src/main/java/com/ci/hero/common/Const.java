package com.ci.hero.common;

public class Const {
    public final static int HP_BOUND_MIN=1;
    public final static int HP_BOUND_MAX=100;

    public final static int ENERGY_BOUND_MIN=0;
    public final static int ENERGY_BOUND_MAX=500;

    public final static int ATTACK_BOUND_MIN=1;
    public final static int ATTACK_BOUND_MAX=20;

    public final static int DEFENSE_BOUND_MIN=0;
    public final static int DEFENSE_BOUND_MAX=30;
}
