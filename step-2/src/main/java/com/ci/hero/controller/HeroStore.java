package com.ci.hero.controller;

import com.ci.hero.model.Hero;
import com.github.cliftonlabs.json_simple.JsonException;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class HeroStore {
    private Map<String, Hero> heroMap;

    public HeroStore() {
        this.heroMap = new HashMap<String, Hero>();
    }

    public int getNbOfHero(){
        return this.heroMap.size();
    }

    public void removeHero(String name) {
        this.heroMap.remove(name);
    }

    public Hero showHero(String name){
        return this.heroMap.get(name);
    }

    public Hero getHero(String name) {
        Hero heTmp = this.heroMap.get(name);
        this.removeHero(name);
        return heTmp;
    }

    public void addHero(Hero he) {
        this.heroMap.put(he.getName(), he);
    }

    public String heroToJsonString(String name) {
        Hero hero = this.getHero(name);
        if (hero == null) {
            return "";
        }
        // Java objects to JSON String
        String json = Jsoner.serialize(hero);

        // pretty print
        String jsonP = Jsoner.prettyPrint(json);
        System.out.println(jsonP);
        return json;
    }

    public boolean heroToJsonFile(String name, File file) {
        Hero hero = getHero(name);
        if (hero == null) {
            return false;
        }
        // Java objects to JSON file
        try {
            FileWriter fileWriter = new FileWriter(file);
            Jsoner.serialize(hero, fileWriter);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Hero jsonFileToHero(File file) {
        Hero hero =null;
        try {
            FileReader fileReader = new FileReader((file));
            JsonObject deserialize = (JsonObject) Jsoner.deserialize(fileReader);

            // need dozer to copy object to staff, json_simple no api for this?
            Mapper mapper = new DozerBeanMapper();
            // JSON to object
            hero = mapper.map(deserialize, Hero.class);
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (JsonException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hero;
    }

}
