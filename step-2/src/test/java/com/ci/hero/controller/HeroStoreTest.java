package com.ci.hero.controller;

import com.ci.hero.model.Hero;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HeroStoreTest {
    private HeroStore hStore;

//    @Rule
//    public TemporaryFolder testFolder = new TemporaryFolder();

    @Before
    public void createHeroStore(){
        this.hStore=new HeroStore();
        for(int i=0;i<50;i++){
            Hero hero1=new Hero("H"+i, "superPower"+i, "elementalAffinity"+i,  "elementalVulnerability"+i , i, i, i, i/2, i, i/2);
            this.hStore.addHero(hero1);
        }
    }

    @After
    public void clearTmpFolder(){
        // Delete all files and folders under the temporary folder.
        File heroJson = new File("src/test/resources/heroStore");
        try {
            FileUtils.cleanDirectory(heroJson);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void addHeroTest(){
        Hero hero =new Hero("NEWH");
        int sizeBefore=this.hStore.getNbOfHero();
        this.hStore.addHero(hero);
        int sizeAfter=this.hStore.getNbOfHero();
        assert(sizeAfter== sizeBefore +1);

        Hero heroBack=this.hStore.showHero("NEWH");
        assert(heroBack!=null);

    }

    @Test
    public void getHeroTest(){
        int sizeBefore=this.hStore.getNbOfHero();
        Hero h=this.hStore.getHero("H10");
        int sizeAfter=this.hStore.getNbOfHero();
        assert(h!=null);
        assert(sizeAfter== sizeBefore -1);
        assert(h.getName().equals("H10"));

        Hero h2=this.hStore.getHero("H10");

        assert(h2==null);
    }

    @Test
    public void removeHeroTest(){
        int sizeBefore=this.hStore.getNbOfHero();
        this.hStore.removeHero("H10");
        int sizeAfter=this.hStore.getNbOfHero();
        assert(sizeAfter== sizeBefore -1);
        Hero h2=this.hStore.getHero("H10");
        assert(h2==null);
    }

    @Test
    public void heroToJsonStringTest(){
        String expectedJson="{\"defense\":10,\"defenseStd\":5.0,\"elementalAffinity\":\"elementalAffinity10\",\"attack\":10,\"name\":\"H10\",\"hp\":10,\"superPower\":\"superPower10\",\"elementalVulnerability\":\"elementalVulnerability10\",\"energy\":10,\"attackStd\":5.0}";
        String resultJson=this.hStore.heroToJsonString("H10");
        System.out.println("Hero h0 json:"+resultJson);
        assert(expectedJson.equals(resultJson));
    }

    @Test
    public void heroToJsonFileStringTest(){
        File heroJson = new File("src/test/resources/heroStore/H10-Hero.json");
        this.hStore.heroToJsonFile("H10",heroJson);
        assert(true);
    }

    @Test
    public void JsonFileToHeroTest(){
        try {
            Hero originalHero=this.hStore.showHero("H10");

            File heroJson = new File("src/test/resources/heroStore/testJson");
            heroJson.createNewFile();

            FileWriter fileWriter = new FileWriter(heroJson);
//            File heroJson = testFolder.newFile("h10-hero.json");
//            FileWriter fileWriter = new FileWriter(heroJson);
            fileWriter.write("{\"defense\":10,\"defenseStd\":5.0,\"elementalAffinity\":\"elementalAffinity10\",\"attack\":10,\"name\":\"H10\",\"hp\":10,\"superPower\":\"superPower10\",\"elementalVulnerability\":\"elementalVulnerability10\",\"energy\":10,\"attackStd\":5.0}");
            fileWriter.close();
            //this.hStore.heroToJsonFile("H10",heroJson);
            Hero hero=this.hStore.jsonFileToHero(heroJson);
            assert(hero!=null);
            assert(originalHero.equals(hero));
        } catch (IOException e) {
            e.printStackTrace();
            assert(false);
        }

    }
}
