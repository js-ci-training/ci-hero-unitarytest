# Advanced Test cases

Get the content of the **step-1** as starting point

## 1 Add features to the Hero project

### 1.1 Add an HeroStore
In order to get a list of heros available, a new class **HeroStore** needs to be created into a new package **com.ci.controller**

**HeroStore** class:

```java
package com.ci.hero.controller;

import ...

public class HeroStore {
    private Map<String, Hero> heroMap;

    public HeroStore() {
        this.heroMap = new HashMap<String, Hero>();
    }

    public int getNbOfHero(){
        return this.heroMap.size();
    }

    public void removeHero(String name) {
        this.heroMap.remove(name);
    }

    public Hero showHero(String name){
        return this.heroMap.get(name);
    }

    public Hero getHero(String name) {
        Hero heTmp = this.heroMap.get(name);
        this.removeHero(name);
        return heTmp;
    }

    public void addHero(Hero he) {
        this.heroMap.put(he.getName(), he);
    }

    public String heroToJsonString(String name) {
        Hero hero = this.getHero(name);
        if (hero == null) {
            return "";
        }
        // Java objects to JSON String
        String json = Jsoner.serialize(hero);

        // pretty print
        String jsonP = Jsoner.prettyPrint(json);
        System.out.println(jsonP);
        return json;
    }

    public boolean heroToJsonFile(String name, File file) {
        Hero hero = getHero(name);
        if (hero == null) {
            return false;
        }
        // Java objects to JSON file
        try {
            FileWriter fileWriter = new FileWriter(file);
            Jsoner.serialize(hero, fileWriter);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Hero jsonFileToHero(File file) {
        Hero hero =null;
        try {
            FileReader fileReader = new FileReader((file));
            JsonObject deserialize = (JsonObject) Jsoner.deserialize(fileReader);

            // need dozer to copy object to staff, json_simple no api for this?
            Mapper mapper = new DozerBeanMapper();
            // JSON to object
            hero = mapper.map(deserialize, Hero.class);
            fileReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (JsonException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hero;
    }

}

```

Explanation :
```java
public class HeroStore {
    private Map<String, Hero> heroMap;

    public HeroStore() {
        this.heroMap = new HashMap<String, Hero>();
    }
```

```private Map<String, Hero> heroMap;``` **Hero** objects are stored into a map with the **Hero** name (String) as Key.

```this.heroMap = new HashMap<String, Hero>();``` creates an instance of the map.


```java
public String heroToJsonString(String name) {
        Hero hero = this.getHero(name);
        if (hero == null) {
            return "";
        }
        // Java objects to JSON String
        String json = Jsoner.serialize(hero);

        // pretty print
        String jsonP = Jsoner.prettyPrint(json);
        System.out.println(jsonP);
        return json;
    }
```

Convert an existing **Hero** object into a json String. The dependency **com.github.cliftonlabs json-simple** (pom.xml) is used to get tools converting Object\* into json string ```String json = Jsoner.serialize(hero); ```

\* Note the object to convert needs to implement ```Jsonable``` and associated functions (```toJson()```) to be converted

```java
public boolean heroToJsonFile(String name, File file) {
        Hero hero = getHero(name);
        if (hero == null) {
            return false;
        }
        // Java objects to JSON file
        try {
            FileWriter fileWriter = new FileWriter(file);
            Jsoner.serialize(hero, fileWriter);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
```

Convert an existing **Hero** into a json file.
```FileWriter fileWriter = new FileWriter(file);``` create a tool file writer used to create and fill a file.
```Jsoner.serialize(hero, fileWriter);``` use the **json-simple** to fill the file with the **Hero** object converted into Json.
```fileWriter.close();``` close the writter file stream to release the file resource.


```java
public Hero jsonFileToHero(File file) {
        Hero hero =null;
        try {
            FileReader fileReader = new FileReader((file));
            JsonObject deserialize = (JsonObject) Jsoner.deserialize(fileReader);

            Mapper mapper = new DozerBeanMapper();
            // JSON to object
            hero = mapper.map(deserialize, Hero.class);
            fileReader.close();
        } catch (FileNotFoundException e) {
            ...

}
```

Read the content of a file and convert it into an **Hero** java object. The dependency **net.sf.dozer dozer** (pom.xml) is used to make this convertion.

```FileReader fileReader = new FileReader((file));``` create a tool file writer used to read a file.

```JsonObject deserialize = (JsonObject) Jsoner.deserialize(fileReader);``` read the file content and convert it into a generic Json Object

```hero = mapper.map(deserialize, Hero.class);``` convert the generic Json object into the Hero Object


### 1.2 Update the hero class

To be translated into json, the **Hero** class needs to be modified as follow:

```java
package com.ci.hero.model;

import ...

public class Hero implements Jsonable {
    ...

    public Hero() {
        this.rand = new Random();
    }

    ...
    // form simple json requirements
    public String toJson() {
        final StringWriter writable = new StringWriter();
        try {
            this.toJson(writable);
        } catch (final IOException e) {
        }
        return writable.toString();
    }

    ...

    public void toJson(Writer writer) throws IOException {
        final JsonObject json = new JsonObject();
        json.put("name", this.getName());
        json.put("superPower", this.getSuperPower());
        json.put("elementalAffinity", this.getElementalAffinity());
        json.put("elementalVulnerability", this.getElementalVulnerability());
        json.put("hp", this.getHp());
        json.put("energy", this.getEnergy());
        json.put("attack", this.getAttack());
        json.put("attackStd", this.getAttackStd());
        json.put("defense", this.getDefense());
        json.put("defenseStd", this.getDefenseStd());
        json.toJson(writer);
    }

    ...
}

```

Explanation

```java
public String toJson() {
        final StringWriter writable = new StringWriter();
        try {
            this.toJson(writable);
        } catch (final IOException e) {
        }
        return writable.toString();
    }
```

Allow to create a writter (String) on demand

```java
 public void toJson(Writer writer) throws IOException {
        final JsonObject json = new JsonObject();
        json.put("name", this.getName());
        json.put("superPower", this.getSuperPower());
        json.put("elementalAffinity", this.getElementalAffinity());
        json.put("elementalVulnerability", this.getElementalVulnerability());
        json.put("hp", this.getHp());
        json.put("energy", this.getEnergy());
        json.put("attack", this.getAttack());
        json.put("attackStd", this.getAttackStd());
        json.put("defense", this.getDefense());
        json.put("defenseStd", this.getDefenseStd());
        json.toJson(writer);
    }
```

Create a Json object with the selected **Hero** attributes


## 2 Create a new Test Case file

### 2.1 Test case preparation

* Create the folder **resources/heroStore** into the test folder
* Create a package into the **test** folder **com.ci.hero.controller**
* Create a test case file **HeroStoreTest** as follow
  
```java
package com.ci.hero.controller;

import ...

public class HeroStoreTest {
    private HeroStore hStore;

//    @Rule
//    public TemporaryFolder testFolder = new TemporaryFolder();

    @Before
    public void createHeroStore(){
        this.hStore=new HeroStore();
        for(int i=0;i<50;i++){
            Hero hero1=new Hero("H"+i, "superPower"+i, "elementalAffinity"+i,  "elementalVulnerability"+i , i, i, i, i/2, i, i/2);
            this.hStore.addHero(hero1);
        }
    }

    @After
    public void clearTmpFolder(){
        // Delete all files and folders under the temporary folder.
        File heroJson = new File("src/test/resources/heroStore");
        try {
            FileUtils.cleanDirectory(heroJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addHeroTest(){
        //todo
    }

    @Test
    public void getHeroTest(){
        //todo
    }

    @Test
    public void removeHeroTest(){
        //todo
    }

    @Test
    public void heroToJsonStringTest(){
        //todo
    }

    @Test
    public void heroToJsonFileStringTest(){
        //todo
    }

    @Test
    public void JsonFileToHeroTest(){
        //todo

    }
}

  ```

Explanation:
```java
@Before
    public void createHeroStore(){
        this.hStore=new HeroStore();
        for(int i=0;i<50;i++){
            Hero hero1=new Hero("H"+i, "superPower"+i, "elementalAffinity"+i,  "elementalVulnerability"+i , i, i, i, i/2, i, i/2);
            this.hStore.addHero(hero1);
        }
    }
```
Create a new **HeroStore** and fill it with fake **Hero**s

```java
    @After
    public void clearTmpFolder(){
        // Delete all files and folders under the temporary folder.
        File heroJson = new File("src/test/resources/heroStore");
        try {
            FileUtils.cleanDirectory(heroJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

Remove all files into the **src/test/resources/heroStore** directory.

### 2.2 test case

Complete the test case file **HeroStoreTest** to test the following cases (update **HeroStore** and **Hero** is needed)
- **addHeroTest** check if a **Hero** can be added, 
  - check for an existing **Hero**, 
  - check for null value send as an **Hero**

* **getHeroTest** check that an Hero can be retreived,
  - check for an existing **Hero**, 
  - check for a missing **Hero**, 
  - check for null value send as an **Hero** name

* **removeHeroTest** check if an **Hero** can be removed from the HeroStore,
  - check for an existing **Hero**, 
  - check for a missing **Hero**, 
  - check for null value send as an **Hero** name

* **heroToJsonStringTest** check the conversion of an **Hero** into a Json String,
  - check for an existing **Hero**, 
  - check for a missing **Hero**, 
  - check for null value send as an **Hero** name

* **heroToJsonFileStringTest** check if an **Hero** can be saved into a file 
  - check for an existing **Hero**, 
  - check for a missing **Hero**, 
  - check for null value send as an **Hero** name
  - check for a non existing targeted folder

* **JsonFileToHeroTest** check the conversion of a **Hero** json file to a **Hero** java object
  - check for an existing **Hero** json file, 
  - check for a missing **Hero** json file, 
  - check for a file that do not contain a **Hero json**
  